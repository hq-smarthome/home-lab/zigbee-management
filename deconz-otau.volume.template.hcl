id = "deconz-otau"
name = "deconz-otau"
type = "csi"
external_id = "deconz-otau"
plugin_id = "axion-proxima"
capacity_max = "2G"
capacity_min = "1G"

capability {
  access_mode     = "single-node-reader-only"
  attachment_mode = "file-system"
}

capability {
  access_mode     = "single-node-writer"
  attachment_mode = "file-system"
}

mount_options {
  fs_type = "cifs"
  mount_flags = [
    "username=[[ .volumeUser ]]",
    "password=[[ .volumePass ]]",
    "vers=3",
    "uid=1005",
    "gid=1005",
    "nolock"
  ]
}

secrets {
  username = "[[ .volumeUser ]]"
  password = "[[ .volumePass ]]"
}

context {
  node_attach_driver = "smb"
  provisioner_driver = "smb-driver"
  server = "[[ .axionServer ]]"
  share = "[[ .axionShare ]]"
}
