id = "deconz-data"
name = "deconz-data"
type = "csi"
external_id = "deconz-data"
plugin_id = "axion-proxima"
capacity_max = "2G"
capacity_min = "1G"

capability {
  access_mode     = "single-node-reader-only"
  attachment_mode = "file-system"
}

capability {
  access_mode     = "single-node-writer"
  attachment_mode = "file-system"
}

mount_options {
  fs_type = "cifs"
  mount_flags = [
    "username=nomad",
    "password=[[ .volumePass ]]",
    "vers=3",
    "uid=1005",
    "gid=1005",
    "nolock"
  ]
}

secrets {
  username = "nomad"
  password = "[[ .volumePass ]]"
}

context {
  node_attach_driver = "smb"
  provisioner_driver = "smb-driver"
  server = "192.168.20.201"
  share = "Proxima/volumes/v/deconz-data"
}
