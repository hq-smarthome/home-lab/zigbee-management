job "zigbee-management" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  group "deconz" {
    count = 1

    volume "deconz-data" {
      type = "host"
      source = "deconz-data"
      read_only = false
    }

    volume "deconz-otau" {
      type = "host"
      source = "deconz-otau"
      read_only = true
    }

    network {
      port "dashboard" { to = 80 }
      port "websocket" { to = 8008 }
      port "vnc" { to = 5900 }
      port "novnc" { to = 6080 }
    }

    task "deconz" {
      driver = "docker"

      volume_mount {
        volume = "deconz-data"
        destination = "/opt/deCONZ"
        read_only = false
      }

      volume_mount {
        volume = "deconz-otau"
        destination = "/root/otau"
        read_only = true
      }

      config {
        image = "deconzcommunity/deconz:stable"
        force_pull = true

        ports = ["dashboard", "websocket", "vnc", "novnc"]

        devices = [
          {
            host_path = "/dev/ttyACM0"
            container_path = "/dev/ttyACM0"
          }
        ]
      }

      template {
        data = <<EOH
          TZ='[[ .defaultTimezone ]]'
          DECONZ_DEVICE=/dev/ttyACM0
          DECONZ_WEB_PORT={{ env "NOMAD_PORT_dashboard" }}
          DECONZ_WS_PORT={{ env "NOMAD_PORT_websocket" }}

          DECONZ_VNC_MODE=1
          DECONZ_VNC_PORT={{ env "NOMAD_PORT_vnc" }}
          DECONZ_NOVNC_PORT={{ env "NOMAD_PORT_novnc" }}

          DECONZ_UPNP=0

          DEBUG_INFO=1
          DEBUG_APS=0
          DEBUG_ZCL=0
          DEBUG_ZDP=0
          DEBUG_OTAU=0
          DEBUG_ERROR=1
          DEBUG_HTTP=0
        EOH

        destination = "secrets/deconz.env"
        env = true
      }

      resources {
        cpu = 512
        device "1cf1/usb/0030" {}
      }

      service {
        name = "deconz-dashboard"
        port = "dashboard"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.deconz.entrypoints=https",
          "traefik.http.routers.deconz.rule=Host(`deconz.hq.carboncollins.se`)",
          "traefik.http.routers.deconz.tls=true",
          "traefik.http.routers.deconz.tls.certresolver=lets-encrypt",
          "traefik.http.routers.deconz.tls.domains[0].main=*.hq.carboncollins.se"
        ]

        check {
          name = "deCONZ Dashboard"
          port = "dashboard"
          type = "tcp"
          interval = "30s"
          timeout = "5s"
          task = "deconz"
        }
      }

      service {
        name = "deconz-websocket"
        port = "websocket"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.deconz-ws.entrypoints=althttps",
          "traefik.http.routers.deconz-ws.rule=Host(`deconz.hq.carboncollins.se`)",
          "traefik.http.routers.deconz-ws.tls=true",
          "traefik.http.routers.deconz-ws.tls.certresolver=lets-encrypt",
          "traefik.http.routers.deconz-ws.tls.domains[0].main=*.hq.carboncollins.se"
        ]

        check {
          name = "deCONZ WebSocket"
          port = "websocket"
          type = "tcp"
          interval = "30s"
          timeout = "5s"
          task = "deconz"
        }
      }
    }

    task "log-shipper-deconz" {
      driver = "docker"

      lifecycle {
        hook = "prestart"
        sidecar = true
      }

      config {
        image = "grafana/promtail:2.4.1"

        args = [
          "-config.file=${NOMAD_TASK_DIR}/promtail.yaml"
        ]
      }

      resources {
        cpu = 50
        memory = 75
      }

      template {
        data =<<EOH
[[ fileContents "./config/promtail.template.yaml" ]]
        EOH

        destination = "local/promtail.yaml"
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
